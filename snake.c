#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>
#include <termios.h>
#include <time.h>
#include <sys/syscall.h>

#define _GNU_SOURCE
#define __NR_myrand 314
#define MAX_SNAKE_LENGTH	50
#define MAX_CBUF_SIZE		(MAX_SNAKE_LENGTH + 1)
#define INITIAL_SIZE		5
struct POINT{
	int x, y;
};
struct C_BUFFER{
	struct POINT buf[MAX_CBUF_SIZE];
	int head, tail;
};
int WIDTH, HEIGHT, mode, over1 = 0, over2 = 0, count1 = 0, count2 = 0;
struct C_BUFFER snake1, snake2;
struct POINT pt1, pt2, junk, player1, player2, move1, move2, pmove1, pmove2, food1, food2;
char **map, **map2;

int cbuf_init(struct C_BUFFER *cb)
{
	if(cb == NULL)	return -1;
	memset(cb, 0, sizeof(struct C_BUFFER));
	return 0;
}
int cbuf_push(struct C_BUFFER *cb, struct POINT *p)
{
	if(cb == NULL || p == NULL)	return -1;

	if((cb->head + 1) % MAX_CBUF_SIZE == cb->tail)	return -1;	// buffer full
	cb->buf[cb->head++] = *p;
	if(cb->head == MAX_CBUF_SIZE)	cb->head = 0;

	return 0;
}
int cbuf_pop(struct C_BUFFER *cb, struct POINT *p)
{
	if(cb == NULL || p == NULL)	return -1;

	if(cb->head == cb->tail)	return -1;	// buffer empty
	*p = cb->buf[cb->tail++];
	if(cb->tail == MAX_CBUF_SIZE)	cb->tail = 0;

	return 0;
}
void map_init(char** m)
{
	int i, j;
	for(i=0;i<HEIGHT+2;i++){
		m[i][0] = '#';
		m[i][WIDTH+1] = '#';
		for(j=1;j<=WIDTH;j++){
			m[i][j] = ' ';
		}
	}
	for(i=1;i<=WIDTH;i++){
		m[0][i] = '#';
		m[HEIGHT + 1][i] = '#';
	}
}
void map_display(char** m, char** m2)
{
	int i, j;
	printf("\x1b[2J");	// erase screen
	for(i=0;i<HEIGHT + 2;i++){
		for(j=0;j<WIDTH + 2;j++) printf("%c", m[i][j]);
		if(mode==2){//2P mode, print the map of palyer2
			for(j=0;j<3;j++) printf(" ");
			for(j=0;j<WIDTH + 2;j++) printf("%c", m2[i][j]);
		}
		printf("\n");
	}
	if(mode==1) printf("current scores: %d\n", count1);
	else if(mode==2) printf("current scores: Player1:%d	Player2:%d\n", count1, count2);
}
char getch_unbuf()
{
        char ch;
        int ret;
        struct termios oios, nios;

        if(tcgetattr(0, &oios) < 0)     perror("tcsetattr()");

        nios = oios;
        nios.c_lflag &= ~(ICANON | ECHO); 
        nios.c_cc[VMIN] = 1;
        nios.c_cc[VTIME] = 0;

        if(tcsetattr(0, TCSANOW, &nios) < 0)            perror("tcsetattr ICANON");

        while(1){ 
                if((ret = read(0, &ch, 1)) == 1)        break;
                else if(ret == EINTR)   continue;  
                perror ("read()");
                return (char)-1;
        }
    
        if(tcsetattr(0, TCSADRAIN, &oios) < 0)  perror ("tcsetattr ~ICANON");
        
        return ch;
}
void *snake1Move(void *param)
{
        int i;
	while(1){
		/*delay time*/
		usleep(1000 * 10);
        	if(++i<10)      continue;
        	i = 0;
		/*player1's snake moves a step*/
		player1.x += move1.x;
		player1.y += move1.y;
		/*record the previous move to pmove*/
		pmove1 = move1;
        	if(map[player1.y][player1.x]=='#' || map[player1.y][player1.x]=='X'){//hit the wall or itself
               		over1 = 1;//player1 game over
			break;
        	}
		else if(map[player1.y][player1.x]=='*'){//eat the food
			count1++;
			do{//generate the new food
				syscall(__NR_myrand, 1, 1);
				syscall(__NR_myrand, 1, 2);
				food1.x = syscall(__NR_myrand, 2, 1) % WIDTH + 1;
				food1.y = syscall(__NR_myrand, 2, 2) % HEIGHT + 1;
			}while(map[food1.y][food1.x]=='#' || map[food1.y][food1.x]=='X');
			map[food1.y][food1.x] = '*';
		}
		else{//just move a step
			cbuf_pop(&snake1, &pt1);
			map[pt1.y][pt1.x] = ' ';
		}
		map[player1.y][player1.x] = 'X';
        	pt1 = player1;
        	if(cbuf_push(&snake1, &pt1) == -1){//the buffer of snake1 is full, discard its tail and push again
			cbuf_pop(&snake1, &junk);
			map[junk.y][junk.x] = ' ';
			cbuf_push(&snake1, &pt1);
		}
	}
	pthread_exit(0);
}
void *snake2Move(void *param)
{
        int i;
	while(1){
	/*delay time*/
		usleep(1000 * 10);
		if(++i<10)      continue;
		i = 0;
		/*player2's snake moves a step*/
		player2.x += move2.x;
		player2.y += move2.y;
		/*record the previous move to pmove*/
		pmove2 = move2;
        	if(map2[player2.y][player2.x]=='#' || map2[player2.y][player2.x]=='X'){//hit the wall or itself
			over2 = 1;//player2 game over
        		break;
		}
		else if(map2[player2.y][player2.x]=='*'){//eat the food
			count2++;
                	do{//create a new food for player2
				syscall(__NR_myrand, 1, 3);
				syscall(__NR_myrand, 1, 4);
                        	food2.x = syscall(__NR_myrand, 2, 3) % WIDTH + 1;
                        	food2.y = syscall(__NR_myrand, 2, 4) % HEIGHT + 1;
                	}while(map2[food2.y][food2.x]=='#' || map2[food2.y][food2.x]=='X');
                	map2[food2.y][food2.x] = '*';
		}
		else{
			cbuf_pop(&snake2, &pt2);
			map2[pt2.y][pt2.x] = ' ';
		}
        	map2[player2.y][player2.x] = 'X';
		pt2 = player2;
		if(cbuf_push(&snake2, &pt2) == -1){//the buffer of snake1 is full, discard its tail and push again
                        cbuf_pop(&snake2, &junk);
			map2[junk.y][junk.x] = ' ';
			cbuf_push(&snake2, &pt2);
                }
	}
	pthread_exit(0);
}
void *keyDetect(void *param){
	char key;
	while(1){
		key = getch_unbuf();
		if(mode==1&&over1 || mode==2&&over1&&over2) break;//game over
		/*detect the key and judge which direction to go, but can't go opposite direction*/
		if(key == 'w' && pmove1.y != 1){//player1 go up
			move1.x = 0;
			move1.y = -1;
		}
		else if(key == 's' && pmove1.y != -1){//player1 go down
			move1.x = 0;
			move1.y =1;
		}
		else if(key == 'a' && pmove1.x != 1){//player1 go left
			move1.x = -1;
			move1.y = 0;
		}
		else if(key == 'd' && pmove1.x != -1){//player1 go right
			move1.x = 1;
			move1.y = 0;
		}
		else if(key == 'i' && pmove2.y != 1){//player2 go up
			move2.x = 0;
			move2.y = -1;
		}
		else if(key == 'k' && pmove2.y != -1){//player2 go down
			move2.x = 0;
			move2.y = 1;
		}
		else if(key == 'j' && pmove2.x != 1){//player2 go left
			move2.x = -1;
			move2.y = 0;
		}
		else if(key == 'l' && pmove2.x != -1){//player2 go right
			move2.x = 1;
			move2.y = 0;
		}
	}
	pthread_exit(0);
}
int main(int argc, char* argv[])
{
	if(argc!=3){
		fprintf(stderr,"Wrong input!\nUsage: ./snake <height> <width>\n");
		return -1;
	}
	HEIGHT = atoi(argv[1]);
	WIDTH = atoi(argv[2]);
	if(HEIGHT<=0 || WIDTH<=INITIAL_SIZE){
		fprintf(stderr,"Wrong width or height!\n");
		return -1;
	}
	printf("Welcome to play the game!\n");
	printf("There are some instructions to controll direction: \'up\',\'down\',\'left\',\'right\'\n");
	printf("   Player1: \'w\',\'s\',\'a\',\'d\'   Player2: \'i\',\'k\',\'j\',\'l\'\n");
	printf("Please choose which mode you want. 1 for 1P, 2 for 2P: ");
	scanf("%d", &mode);
	while(mode<1 || mode>2){
		printf("You must input 1 or 2! Please input again: ");
		scanf("%d", &mode);
	}
	int i;
        pthread_t key_tid, snake1_tid, snake2_tid;
        pthread_attr_t key_attr, snake1_attr, snake2_attr;
	pthread_attr_init(&key_attr);//get the default attributes
	pthread_attr_init(&snake1_attr);
	if(mode==2) pthread_attr_init(&snake2_attr);
	pthread_create(&key_tid, &key_attr, keyDetect, NULL);//create the thread for detecting the input from keyboard
	/*create a dynamic array as the map*/
	char* pData;
	map = (char**)malloc((HEIGHT+2)*sizeof(char*) + (HEIGHT+2)*(WIDTH+2)*sizeof(char));
	for(i=0,pData=(char*)(map+HEIGHT+2);i<HEIGHT+2;i++,pData=pData+WIDTH+2) map[i]=pData;
	if(mode==2){
		map2 = (char**)malloc((HEIGHT+2)*sizeof(char*) + (HEIGHT+2)*(WIDTH+2)*sizeof(char));
        	for(i=0,pData=(char*)(map2+HEIGHT+2);i<HEIGHT+2;i++,pData=pData+WIDTH+2) map2[i]=pData;
	}
	/*initialize for player1*/
	/*set the seed for random number generator*/
        syscall(__NR_myrand, 1, 1);
        syscall(__NR_myrand, 1, 2);
	map_init(map);
	cbuf_init(&snake1);
	move1.x = pmove1.x = 1;
	move1.y = pmove1.y = 0;
	player1.y = 1;
	player1.x = INITIAL_SIZE;
	pt1.y = player1.y;
	if(mode==2){//initialize for player2
		map_init(map2);
		cbuf_init(&snake2);
		move2.x = pmove2.x = 1;
		move2.y = pmove2.y = 0;
		player2.y = 1;
		player2.x = INITIAL_SIZE;
		pt2.y = player2.y;
	}
	for(i=1;i<=INITIAL_SIZE;i++){//initialize both of snakes with the lenght of INITIAL_SIZE
		map[player1.y][i] = 'X';
		pt1.x = i;
		cbuf_push(&snake1, &pt1);
		if(mode==2){
			map2[player2.y][i] = 'X';
			pt2.x = i;
			cbuf_push(&snake2, &pt2);
		}
	}
	do{//put the food for player1
		food1.x = syscall(__NR_myrand, 2, 1) % WIDTH + 1;
		food1.y = syscall(__NR_myrand, 2, 2) % HEIGHT + 1;
        }while(map[food1.y][food1.x]=='#' || map[food1.y][food1.x]=='X');
	map[food1.y][food1.x] = '*';
        if(mode==2){
		/*set the seed for random number generator*/
		syscall(__NR_myrand, 1, 3);
        	syscall(__NR_myrand, 1, 4);
		do{//put the food for player2
			food2.x = syscall(__NR_myrand, 2, 3) % WIDTH + 1;
                	food2.y = syscall(__NR_myrand, 2, 4) % HEIGHT + 1;
        	}while(map2[food2.y][food2.x]=='#' || map2[food2.y][food2.x]=='X');
		map2[food2.y][food2.x] = '*';
	}
	i = 0;
	pthread_create(&snake1_tid, &snake1_attr, snake1Move, NULL);//create a thread for palyer1 to play
        if(mode==2) pthread_create(&snake2_tid, &snake2_attr, snake2Move, NULL);//create a thread for player2 to play
	while(1)
	{
		map_display(map,map2);
		/*Delay Time*/
		usleep(1000 * 10);
                if(++i<10)      continue;
                i = 0;
		if(mode==1&&over1 || mode==2&&over1&&over2) break;//game over
	}
	pthread_join(snake1_tid, NULL);
	if(mode==2) pthread_join(snake2_tid, NULL);
	printf("Game over!\n");
	if(mode==2){
		if(count1==count2) printf("Tie game!\n");
		else printf("%s won!\n",count1>count2?"Player1":"Player2");
	}
	pthread_join(key_tid, NULL);
	return 0;
}
